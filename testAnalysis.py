import couchdb
#from __future__ import print_function
import semantria
#import uuid
import time
#import tweepy
#import sys
#reload(sys)
#sys.setdefaultencoding('utf-8')

couch = couchdb.Server('http://localhost:5984')

db = couch['tweetharvest2']
#name = 'Joanne Smith'
#user = tweepy.api.get_user(name)
#user_id =user.id

results = db.view('test/testday')

user_id = 3137279134


time1 = time.time()

serializer = semantria.JsonSerializer()
session = semantria.Session("b0f2c6ef-4606-4bc5-be82-cb8fe0a2ded9", "42f1079d-50d6-4277-a826-1870a90cf6eb", serializer, use_compression=True)
i = 0
days = {}
for result in results:
	if i >5:
		break
	days[result.key[0]]=result.value
	#print result.key[1]
	doc = {"id": result.key[0].replace("-", ""), "text": result.key[1]}
	status = session.queueDocument(doc)
	if status == 202:
		print("\"", doc["id"], "\" document queued successfully.", "\r\n")
	#print '  day: '+result.value
	i+=1
#print i
length = 5
results1 = []
while len(results1) < length:
   print("Retrieving your processed results...", "\r\n")
   time.sleep(2)
   # get processed documents
   status = session.getProcessedDocuments()
   results1.extend(status)

for data in results1:
   # print document sentiment score

   print("Document ", data["id"], " Sentiment score: ", data["sentiment_score"], " day", days[data["id"]], "\r\n")



time2 = time.time()
print str(time2 - time1)