apt-get -f install
apt-get install -y build-essential
apt-get install -y erlang-base erlang-dev erlang-nox erlang-eunit
apt-get install -y libmozjs185-dev libicu-dev libcurl4-gnutls-dev libtool
cd ..
cd /usr/local/src
curl -O http://apache.mirrors.tds.net/couchdb/source/1.6.1/apache-couchdb-1.6.1.tar.gz
$ tar -xzf apache-couchdb-1.6.1.tar.gz
cd apache-couchdb-1.6.1/
./configure
make && make install
service couchdb start